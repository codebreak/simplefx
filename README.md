<h1><img src="https://gitlab.com/benjamin.lemelin/simplefx/-/raw/master/.docs/Logo.svg" alt="SimpleFx" height="100" /></h1>

SimpleFX is wrapper arround JavaFX that provides some useful features. It provides some useful features and helps with
some basic object oriented principles.

## Changelog

See the [CHANGELOG.md](CHANGELOG.md) file for version details.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details